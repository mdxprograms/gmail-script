import smtplib
import getpass
try:
    from validate_email import validate_email
except ImportError:
    print "Please install validate_email -- sudo pip install validate_email"

def send_mail(sender, recipient, subject, body, password):
    SMTP_SERVER = 'smtp.gmail.com'
    SMTP_PORT = 587
    # format and send email

    body = "" + body + ""

    headers = ["From " + sender,
               "Subject: " + subject,
               "To: " + recipient,
               "MIME-VERSION: 1.0",
               "Content-Type: text/html"]
    headers = "\r\n".join(headers)

    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)

    session.ehlo()
    session.starttls()
    session.ehlo()
    session.login(sender, password)

    session.sendmail(sender, recipient, headers + "\r\n\r\n" + body)
    session.quit()

    print "Mail to " + recipient + " has been sent."


def main():
    rec = raw_input("To: ")
    sender = raw_input("From: ")
    subject = raw_input("Subject: ")
    body = raw_input("Body: ")
    password = getpass.getpass("Password: ")

    validate_sender = validate_email(sender)
    validate_rec = validate_email(rec)

    errors = []

    if not validate_rec:
        errors.append("Invalid Recipient email")
    elif not validate_sender:
        errors.append("Invalid Sender email")
    elif not subject:
        errors.append("Subject left empty.")
    elif not body:
        errors.append("Body left empty")
    elif not password:
        errors.append("Password left empty")
    else:
        send_mail(sender, rec, subject, body, password)

    print errors

if __name__ == "__main__":
    main()